//load the libraries
var path = require("path");
var express = require("express");

//create an instance of the application
var app =express();

//master regist
var regist = [];

var createReg = function(ctrl) {
	    return ({
		        email: ctrl.email,
                password: ctrl.password,
                name: ctrl.name,
                gender: ctrl.gender,
                dob: ctrl.dob,
                addr: ctrl.addr,
                ctry: ctrl.ctry,
                contact: ctrl.contact,
	            });
        }


//Define routes

app.get("/register-now",function(req,resp){

    //add into regist array to store the information
    regist.push(createReg(req.query));  
    console.log("All registeration:\n %s", JSON.stringify
    (regist));

    resp.status(201).end();
});

app.get("/display-regist", function(req,resp){
    resp.status(200);
    resp.type("application/json");
    resp.json(regist);

} );






app.use("/libs", express.static(path.join(__dirname, "bower_components")));

app.use(express.static(path.join(__dirname,"public")));

//Setup the server
app.set("port", process.env.APP_PORT || 3000);

app.listen(app.get("port"), function(){
    console.log("Application started at %s on port %d", new Date(), app.get("port"));

});

