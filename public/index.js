// Registration Page

(function(){
    var RegApp = angular.module("RegApp",[]);

        var initForm = function(ctrl){
            ctrl.email ="";
            ctrl.password ="";
            ctrl.name ="";
            ctrl.gender ="";
            ctrl.dob ="";
            ctrl.addr ="";
            ctrl.ctry ="";
            ctrl.contact ="";
            };

        var createRegObj = function(ctrl) {
            // ctrl.dob = ctrl.dob.toLocaleDateString();

		    return ({
                email: ctrl.email,
                password: ctrl.password,
                name: ctrl.name,
                gender: ctrl.gender,
                dob: ctrl.dob,
                addr: ctrl.addr,
                ctry: ctrl.ctry,
                contact: ctrl.contact,
		        });
	        }

        

    // contoller start here
    
    var RegCtrl = function($http){
        var regCtrl = this;


        initForm(regCtrl);

    //  calculating age   
        var minAge = 18;
        var curDate = new Date();
        var curr_year = curDate.getFullYear();
        var curr_month = curDate.getMonth();

    //Calculates age from given Birth Date in the form//
        function _calcAge() {

            var dt1 = regCtrl.dob;
            var birth_date = new Date(dt1);
               
            var birth_year = birth_date.getFullYear();
            var birth_month = birth_date.getMonth();
            var calc_year = curr_year - birth_year;
            var calc_month = curr_month - birth_month;
            var totalmth = calc_year*12 + calc_month;
            console.log("Total month is %d", totalmth);
                if (totalmth < 216){
                    alert("Only above 18 years old allow to register");
                    initForm(regCtrl);
                } 
                
            };

   
// end of check age function

// start of check phone function
   function _phoneChk(str) {
    var patt1 = /[*&&%#|\/{}@$![]]/g;
    var result = str.match(patt1);
  if (result) {
            alert("Contact number should not contain invalid characters");
             initForm(regCtrl);           
            }
  else{ console.log("Phone number valid")};
};
                    
// end of check phone function

// password validation
 function _pwChk(str) {
    var patt1 = /[@#$0123456789]/g;
    var result = str.match(patt1);
    if (result) { 
                console.log("Password  valid");   
                }
    else{ 
        alert(" number should not contain invalid characters")
    };
};





        regCtrl.register = function() {
                _calcAge();  // validate Age above 18
                _phoneChk(regCtrl.contact); // Validate Phone no invalid character
                _pwChk(regCtrl.password);  // Validate password

                if(regCtrl.email!==""){
                var regDetail = createRegObj(regCtrl);
               ($http.get("/register-now", {
				params: regDetail
			    }));
                };
		};






    };






    RegApp.controller("RegCtrl", [ "$http", RegCtrl] );

})();







